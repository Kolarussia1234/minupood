<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class MinuMoodul extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'MinuMoodul';
        $this->tab = 'checkout';
        $this->version = '1.0.0';
        $this->author = 'Nick Ovt';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('TextModuleNick');
        $this->description = $this->l('Here is my new great module for Prestashop! See on minu moodul. VP_');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall my module?');

        if (!Configuration::get('MYMODULE_NAME'))
      $this->warning = $this->l('No name provided');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('MINUMOODUL_LIVE_MODE', true);

    	if (Shop::isFeatureActive())
{
  Shop::setContext(Shop::CONTEXT_ALL);
}

        return parent::install() &&
        	$this->registerHook('displayProductAdditionalInfo')&&
        	$this->registerHook('leftColumn')&&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayRightColumnProduct');
    }

    public function uninstall()
    {
        Configuration::deleteByName('MINUMOODUL_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {

        /**
         * If values have been submitted in the form, process.
         */
        $output1 = null;

        if (((bool)Tools::isSubmit('submitMinuMoodulModule')) == true) {
        	$my_module_name = strval(Tools::getValue('MYMODULE_NAME'));
        if (!$my_module_name
          || empty($my_module_name)
          || !Validate::isGenericName($my_module_name))
            $output1 .= $this->displayError($this->l('Invalid Configuration value'));
        else
        {
            Configuration::updateValue('MYMODULE_NAME', $my_module_name);
            $output1 .= $this->displayConfirmation($this->l('Settings updated'));
        }

            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$output1.$this->renderForm();
    }





    /**
     * Create the form that will be displayed in the configuration of your module.
     *Konfiala
     */
   protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = true;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 1);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMinuMoodulModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', true)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     *Konfiala struktuur
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                	array(
                		'type' => 'text',
                		'label' => $this->l('Name your module'),
                    'desc' => $this->l('This will be the name that will be shown near Product Description');
                		'name' => 'MYMODULE_NAME'
                	),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'MINUMOODUL_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            ),
                        ),
                    ),
                    array(
                    	'col' => 3,
                    	'type' => 'text',
                    	'desc' => $this->l('Enter text displayed on the Product Description'),
                    	'name' => 'MINUMOODUL_PRODUCT_TEXT',
                    	'label' => $this->l('Product description'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
        	'MYMODULE_NAME' => Configuration::get('MYMODULE_NAME'),
            'MINUMOODUL_LIVE_MODE' => Configuration::get('MINUMOODUL_LIVE_MODE', true),
            'MINUMOODUL_PRODUCT_TEXT' => Configuration::get('MINUMOODUL_PRODUCT_TEXT')
        );
    }

    /**
     * Save form data.
     * Konfialas sisestatud andmete salvestamine
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }

    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
      //Praegu tyhjad, sest nad pole vajalikud, veel.
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {

      //Praegu tyhjad, sest nad pole vajalikud, veel.
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }


      //////Jatan, kuna voib vajalikuks minna.//////
    /*public function hookDisplayLeftColumn($params)
{
  $this->context->smarty->assign(
      array(
          'my_module_name' => Configuration::get('MYMODULE_NAME'),
          'my_module_link' => $this->context->link->getModuleLink($this->name, 'display'),
          'my_module_message' => $this->l('This is a simple text message') // Do not forget to enclose your strings in the l() translation method
      )
  );
  return $this->display(__FILE__, 'views/templates/hook/mymodule.tpl');
} */
 
 
public function hookDisplayHeader()
{
  //alert.js on yherealine .js fail, kasutasin seda testimiseks

	//$this->context->controller->addJS($this->_path.'/views/js/alert.js');

  //Css faili juurdelisamine, et mymodule.tpl'is saaks HTML'i ilusaks teha :)
    $this->context->controller->addCSS($this->_path.'views/css/mymodule.css', 'all');
}

      //Mooduli pohiline funktsioon, naitab (info)teksti toote all.
      public function hookDisplayProductAdditionalInfo($params){
              $this->context->smarty->assign(
                    array(
                        'my_module_name' => Configuration::get('MYMODULE_NAME'),
                        'my_module_link' => $this->context->link->getModuleLink($this->name, 'display'),
                        'my_module_message' => $this->l('This is a simple text message!Nikolai') // Do not forget to enclose your strings in the l() translation method
                    )
                );
         return $this->display(__FILE__, 'views/templates/hook/mymodule.tpl');
      }

      //Jatan, kuna voib vajalikuks minna
      //Vajadusel voib variables(ei maleta kuidas eesti keeles) vahetada, et naitada midagi muud
   /* public function hookDisplayRightColumnProduct($params){
        $this->context->smarty->assign(
      array(
          'my_module_name' => Configuration::get('MYMODULE_NAME'),
          'my_module_link' => $this->context->link->getModuleLink($this->name, 'display'),
          'my_module_message' => $this->l('This is a simple text message') // Do not forget to enclose your strings in the l() translation method
      )
  );

        return $this->display(__FILE__, 'views/templates/hook/mymodule.tpl');
    } */
}