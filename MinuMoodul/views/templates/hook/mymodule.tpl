<!-- Block mymodule -->
<div id="mymodule_block_home" class="block">
  <h4>
		{l s='Welcome' mod='MinuMoodul'}!
  </h4>
  <div class="block_content">
    <p>
			{l s='Hello' mod='MinuMoodul'},
       {if isset($my_module_name) && $my_module_name}
           {$my_module_name}
       {else}
           {l s='World' mod='MinuMoodul'}
       {/if}
       !
    </p>
    <ul>
	  <li>{$my_module_message}</li>
    </ul>
  </div>
</div> 
<!-- /Block mymodule -->